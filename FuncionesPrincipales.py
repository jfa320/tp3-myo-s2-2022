import multiprocessing
from builtins import print
from Modelo.Droga import Droga
from Modelo.Remedio import Remedio
from Modelo.RemedioCantidad import RemedioCantidad
from pyscipopt import *
import os
import time

cotaMaximaCantRemedios = 0
resultadosValores = ""
resultadosValoresDual = ""
valorObjetivo = 0
valorObjetivoDual = 0
sumaRequeridas = 0
cantidadRemediosTotales = 0
valorObjetivoHalladoSolucionInicial = 0
cantidadRemediosDistintosUsados = 0
drogasTotales = []
valorObjetivoNuevo=0
# Creo el modelo del primal (problema maestro) | lo hago global para acceder desde los metodos que lo modifican
modelPrimal = Model("CoctelDrogasPrimal")
modelDual = Model("CoctelDrogasDual")

def agregarNuevoRemedioPrimal(remedioNuevo, costoEnvasado, nombreVariable, drogasTotales):
    print("------------------------------------------------------------")
    print("Voy a agregar nuevo remedio al primal")
    global modelPrimal
    global modelDual
    # habilito reoptimizacion
    modelPrimal.freeTransform()
    # creo nueva variable
    nuevaVariable = modelPrimal.addVar(nombreVariable, vtype="C") #tipo continua para la relajacion
    # creo nueva funcion objetivo
    foNueva = modelPrimal.getObjective() + (costoEnvasado + remedioNuevo.getCosto()) * nuevaVariable
    modelPrimal.chgReoptObjective(foNueva, "minimize")
    # cuento las restricciones que tengo que no sean de no negatividad (positividad)
    numeroRestriccionesSinPositividad = (modelPrimal.getNVars() - 1)
    # modifico restricciones de cantidad droga necesaria por el paciente
    for cons in range(0, modelPrimal.getNConss() - numeroRestriccionesSinPositividad):
        droga = drogasTotales[cons]
        modelPrimal.addConsCoeff(modelPrimal.getConss()[cons], nuevaVariable, remedioNuevo.cantidadDroga(droga))
    modelPrimal.addCons(nuevaVariable >= 0)

    print("Voy a agregar nueva restriccion al dual")
    # habilito reoptimizacion
    modelDual.freeTransform()
    # creo nueva variable
    # dejo la misma funcion objetivo
    modelDual.chgReoptObjective(modelDual.getObjective(), "maximize")
    drogaRemedio = {}
    numeracionVariablesDual = []
    i = 0
    for droga in drogasTotales:
        drogaRemedio["y" + str(i + 1)] = remedioNuevo.cantidadDroga(droga)
        numeracionVariablesDual.append(i)
        i += 1
    modelDual.addCons(quicksum(drogaRemedio["y" + str(i + 1)] * modelDual.getVars()[i] for i in
                               numeracionVariablesDual) <= costoEnvasado + remedioNuevo.getCosto())

    # optimizo el modelo nuevo
    modelDual.optimize()
    # obtengo soluciones
    sol = modelDual.getBestSol()
    solucionCompletaRemedioNuevo = {}
    for variablesModelo in modelDual.getVars():
        solucionCompletaRemedioNuevo[str(variablesModelo)] = sol[variablesModelo]
    global valorObjetivoNuevo
    valorObjetivoNuevo=modelDual.getObjVal()
    return solucionCompletaRemedioNuevo


def buscarRemedioNuevo(drogasTotales, costoEnvasado, factorMax, solucionDual, cantidadRemediosDistintosUsados):
    print("------------------------------------------------------------")
    print("Voy a buscar un remedio candidato a agregar al modelo")
    # Creo el modelo del primal (problema maestro)
    modelRemedio = Model("BusquedaRemedioCNuevo")

    # --------------------
    # Agrego variables al modelo
    x = {}  # x: uso o no la droga d en el remedio
    print("Empiezo a crear variables")
    numeracionDrogas = []  # me creo un array con los indices de todas las drogas disponibles
    for d in range(0, drogasTotales.__len__()):
        nombreVariable = "x" + str(d + 1)
        x[d] = modelRemedio.addVar(nombreVariable, vtype="BINARY")
        numeracionDrogas.append(d)
    # # --------------------
    print("Defino funcion objetivo")
    modelRemedio.setObjective((quicksum(solucionDual["y" + str(d + 1)] * x[d] for d in numeracionDrogas)) - (
                costoEnvasado + quicksum(drogasTotales[d].getCosto() * x[d] for d in numeracionDrogas)),
                              "maximize")
    # # --------------------
    # # defino restricciones
    # # ---------------------------------

    # restriccion de factor de inestabilidad
    modelRemedio.addCons(
        quicksum(drogasTotales[d].getFactorInestabilidad() * x[d] for d in numeracionDrogas) <= factorMax)
    # ----------------------------------
    modelRemedio.hideOutput(True)
    # optimizo el modelo
    print("Voy a optimizar el modelo BUSCADOR DE REMEDIOS")
    modelRemedio.optimize()
    # obtengo solucion
    solucion = modelRemedio.getBestSol()
    # si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia
    if solucion == "{}":
        resultadosRemedioNuevo = "SOLUCIÓN VACÍA"
        print("SOLUCIÓN VACÍA")
    elif (modelRemedio.getStatus() != "infeasible"):
        print("Se encontró un resultado optimo factible en el BUSCADOR DE REMEDIOS.")
        valorObjetivo = modelRemedio.getObjVal()
        if(valorObjetivo>0): # mi remedio es util solo en el caso que la FO sea positiva
            solucionCompleta = {}
            for variablesModelo in modelRemedio.getVars():
                solucionCompleta[str(variablesModelo)] = solucion[variablesModelo]
            resultadosRemedioNuevo = str(solucionCompleta)

            remedioNuevo = Remedio()
            drogasNuevoRemedio = []
            remedioNuevo.setNombre("r" + str(cantidadRemediosDistintosUsados + 1))
            for d in range(0, drogasTotales.__len__()):
                if(int(solucionCompleta["x" + str(d + 1)]))==1:
                    droga = drogasTotales[d]
                    drogasNuevoRemedio.append(droga)
            remedioNuevo.setDrogas(drogasNuevoRemedio)
            print("------------------------------------------------------------")
            return remedioNuevo
        else:
            print("------------------------------------------------------------")
            print("No encontre otro remedio a agregar")
            return 0
    else:
        resultadosRemedioNuevo = "NO SE ENCONTRARON SOLUCIONES FACTIBLES"
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")
    # --------------------
    print(resultadosRemedioNuevo)
    print("------------------------------------------------------------")


def optimizar():
    print("---------------------------------")
    print("Input: ")
    print("Costo Envasado: K=" + str(costoEnvasado))
    print("Drogas disponibles: D=" + str(drogasDisponibles))
    print("Factor Inestabilidad Maximo: H=" + str(factorMax))
    print("---------------------------------")
    print("Variables: ")
    print("x_r: cantidad de veces que uso el remedio r (r e R)")
    print("---------------------------------")
    global resultadosValores
    global resultadosValoresDual
    global valorObjetivo
    global valorObjetivoDual
    global valorObjetivoHalladoSolucionInicial
    global cantidadRemediosTotales
    global drogasTotales
    global sumaRequeridas
    global cantidadRemediosDistintosUsados
    global valorObjetivoNuevo
    global modelPrimal
    global modelDual
    for droga in drogasTotales:
        sumaRequeridas += droga.getUnidadesRequeridas()
    # --------------------

    # Agrego variables al modelo
    x = {}  # x: cantidades de veces que fabrico el remedio
    print("Empiezo a crear variables")
    numeracionRemedios = []  # me creo un array con los indices de todos los remedios disponibles
    costoRemedio = []

    drogaRemedio = {}  # guardo la cantidad de droga que tiene cada remedio
    for i in range(0, cantidadRemediosDistintosUsados):
        nombreVariable = "x" + str(i + 1)
        # x[i] = modelPrimal.addVar(nombreVariable, vtype="INTEGER")
        x[i] = modelPrimal.addVar(nombreVariable, vtype="C") #la declaro como continua. Relajo el modelo.
        numeracionRemedios.append(i)
        costoRemedio.append(remediosNecesariosSolucionInicial[i].getRemedio().getCosto())
        j = 0
        for droga in drogasTotales:
            drogaRemedio[i, j] = remediosNecesariosSolucionInicial[i].getRemedio().cantidadDroga(droga)
            j += 1

    print("Defino funcion objetivo")
    modelPrimal.setObjective((quicksum((costoEnvasado + costoRemedio[r]) * x[r] for r in numeracionRemedios)),
                             "minimize")
    # # --------------------
    # # defino restricciones
    # # ---------------------------------

    # satisfaga requisitos del paciente
    indiceDroga = 0
    for droga in drogasTotales:
        P_d = droga.getUnidadesRequeridas()
        modelPrimal.addCons(quicksum(drogaRemedio[r, indiceDroga] * x[r] for r in numeracionRemedios) >= P_d)
        indiceDroga += 1
    # no negatividad
    for i in numeracionRemedios:
        modelPrimal.addCons(x[i] >= 0)

    # ----------------------------------
    modelPrimal.hideOutput(True)

    # --------------------
    # Creo el modelo del dual
    print("----------------------------")
    print("Empiezo con el modelo DUAL")

    # --------------------
    # Agrego variables al modelo dual
    y = {}  # x: cantidades de veces que fabrico el remedio
    print("Empiezo a crear variables")

    cantidadRestriccionesPrimal = drogasTotales.__len__()
    numeracionVariablesDual = []
    for i in range(0, cantidadRestriccionesPrimal):
        nombreVariable = "y" + str(i + 1)
        y[i] = modelDual.addVar(nombreVariable, vtype="C") #Continua para relajacion
        numeracionVariablesDual.append(i)
    print("Defino funcion objetivo")
    # P_d = drogasTotales[i].getUnidadesRequeridas() con i en la cantidad de restricciones
    modelDual.setObjective(
        (quicksum(drogasTotales[i].getUnidadesRequeridas() * y[i] for i in numeracionVariablesDual)), "maximize")

    # # --------------------
    # # defino restricciones
    # # ---------------------------------

    # restriccion de variables del primal
    for r in range(0, cantidadRemediosDistintosUsados):
        modelDual.addCons(
            quicksum(drogaRemedio[r, i] * y[i] for i in numeracionVariablesDual) <= costoEnvasado +
            costoRemedio[r])

    # no negatividad
    for i in numeracionVariablesDual:
        modelDual.addCons(y[i] >= 0)

    # ----------------------------------
    modelDual.hideOutput(True)
    # optimizo el modelo
    print("Voy a optimizar el DUAL")
    modelDual.optimize()
    # obtengo solucion
    solucion = modelDual.getBestSol()
    # si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia
    if solucion == "{}":
        resultadosValoresDual = "SOLUCIÓN VACÍA"
        print("SOLUCIÓN VACÍA")
    elif (modelDual.getStatus() != "infeasible"):
        valorObjetivoDual = modelDual.getObjVal()
        solucionCompleta = {}
        for variablesModelo in modelDual.getVars():
            solucionCompleta[str(variablesModelo)] = solucion[variablesModelo]
        resultadosValoresDual = str(solucionCompleta)
        solucionDual = solucionCompleta
    else:
        resultadosValoresDual = "NO SE ENCONTRARON SOLUCIONES FACTIBLES"
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")
    print("----------------------------")
    # ----------------------------------------------------------------------------------

    remedioNuevo = buscarRemedioNuevo(drogasTotales, costoEnvasado, factorMax, solucionDual,
                                     cantidadRemediosDistintosUsados)
    if (remedioNuevo != 0):
        print("devolvi un remedio util")
        print("voy a agregar un nuevo remedio")
        nombreVarNueva = "x" + str(cantidadRemediosDistintosUsados + 1)
        cantidadRemediosDistintosUsados += 1
        solucionDual = agregarNuevoRemedioPrimal(remedioNuevo, costoEnvasado, nombreVarNueva,
                                                 drogasTotales)

    # Logica para cargar remedios nuevos
    while (remedioNuevo != 0):
        print(solucionDual)
        remedioNuevo = buscarRemedioNuevo(drogasTotales, costoEnvasado, factorMax, solucionDual,
                                          cantidadRemediosDistintosUsados)

        if (remedioNuevo != 0):
            print("devolvi un remedio util")
            print("voy a agregar un nuevo remedio")
            print(remedioNuevo)
            nombreVarNueva = "x" + str(cantidadRemediosDistintosUsados + 1)
            cantidadRemediosDistintosUsados += 1
            solucionDualVieja=solucionDual
            solucionDual = agregarNuevoRemedioPrimal(remedioNuevo, costoEnvasado, nombreVarNueva,
                                                     drogasTotales)
            if(solucionDualVieja==solucionDual): #en caso de que las soluciones no mejoren, el sistema intenta agregar
                #el mismo remedio por lo que corto la ejecucion cuando las soluciones son iguales
                break

    # ----------------------------------------------------------------------------------
    # optimizo el modelo
    print("Desactivo la relajacion seteando todas las variables como enteras")
    for varPrimal in modelPrimal.getVars():
        modelPrimal.chgVarType(varPrimal,"INTEGER")
    print("Ahora voy a optimizar el PRIMAL")
    modelPrimal.optimize()
    # obtengo solucion
    solucion = modelPrimal.getBestSol()
    # si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia
    if solucion == "{}":
        resultadosValores = "SOLUCIÓN VACÍA"
        print("SOLUCIÓN VACÍA")
    elif (modelPrimal.getStatus() != "infeasible"):
        print("Se encontró un resultado optimo factible en el PRIMAL. Ver archivo generado para más información.")
        valorObjetivo=modelPrimal.getObjVal()
        print("Valor objetivo: ", valorObjetivo)
        solucionCompleta = {}
        for variablesModelo in modelPrimal.getVars():
            solucionCompleta[str(variablesModelo)] = solucion[variablesModelo]
        resultadosValores = str(solucionCompleta)
        print("Cantidad de remedios considerados en el modelo: ",modelPrimal.getNVars())
        print("Suma de requeridas", sumaRequeridas)
        print("Solucion final:", resultadosValores)
    else:
        resultadosValores = "NO SE ENCONTRARON SOLUCIONES FACTIBLES"
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")


    print("------------------------------------------------------------")


def buscoSolucionInicial():
    # armo remedios que tengan todas las drogas. Con tener n remedios donde n es la maxima cantidad que me piden de una droga
    # ya puedo asegurar que cumplo con los requisitos del paciente
    print("Busco una solucion inicial")
    global factorMax
    global costoEnvasado
    global cantidadRemediosTotales
    global valorObjetivoHalladoSolucionInicial  # guardo el costo de la solucion
    global cantidadRemediosDistintosUsados
    global drogasTotales
    global remediosNecesariosSolucionInicial
    remediosNecesariosSolucionInicial = []
    remediosFabricados = []
    valorObjetivoHalladoSolucionInicial = 0
    cantidadRemediosDistintosUsados = 0
    i = 1
    drogasAgregar = []
    for droga in drogasDisponibles:
        drogasAgregar = []
        factorDrogaActual = drogasDisponibles[droga]["factor de inestabilidad"]
        requeridasDroga = drogasDisponibles[droga]["unidades requeridas"]
        costoActual = drogasDisponibles[droga]["costo unitario"]
        drogaNueva = Droga()
        drogaNueva.setNombre("d" + str(i))
        drogaNueva.setUnidadesRequeridas(requeridasDroga)
        drogaNueva.setCosto(costoActual)
        drogaNueva.setFactorInestabilidad(factorDrogaActual)
        drogasTotales.append(drogaNueva)
        drogasAgregar.append(drogaNueva)
        remedioNuevo = Remedio()
        remedioNuevo.setNombre("r" + str(i))
        remedioNuevo.setDrogas(drogasAgregar)

        remediosFabricados.append(remedioNuevo)
        i += 1

    for remedio in remediosFabricados:
        cantidadRemedio = remedio.getDrogas()[0].getUnidadesRequeridas()
        remedioCantidad = RemedioCantidad()
        remedioCantidad.setRemedio(remedio)
        remedioCantidad.setCantidad(cantidadRemedio)
        remediosNecesariosSolucionInicial.append(remedioCantidad)
    cantRemediosUsados = 0
    for remedioCantidad in remediosNecesariosSolucionInicial:
        cantRemediosUsados += remedioCantidad.getCantidad()
    costoEnvaseRemedios = cantRemediosUsados * costoEnvasado
    valorObjetivoHalladoSolucionInicial = costoEnvaseRemedios
    for remedioCantidad in remediosNecesariosSolucionInicial:
        valorObjetivoHalladoSolucionInicial += remedioCantidad.getRemedio().getCosto() * remedioCantidad.getCantidad()
    cantidadRemediosTotales = cantRemediosUsados
    cantidadRemediosDistintosUsados = remediosNecesariosSolucionInicial.__len__()
    print("Cantidad remedios Usados en la solucion inicial: ", cantidadRemediosTotales)
    print("Cantidad remedios DISTINTOS Usados en la solucion inicial: ", cantidadRemediosDistintosUsados)
    print("Valor Objetivo Hallado Solucion Inicial", valorObjetivoHalladoSolucionInicial)


def Coctel(farmacia, remedio):
    # creo una variable del momento en que inicia el metodo
    inicioTiempo = time.time()
    global remedioNuevo
    # Uso el mismo metodo para cargar tanto la droga como el remedio
    leerTxtFarmacia(farmacia)  # lectura del primer archivo ("drogas")
    leerTxtFarmacia(remedio)  # lectura del primer archivo ("remedios")
    extraerFactorInestabilidadMax()  # extraigo el factor de inestabilidad maximo
    extraerCostoEnvasado()  # extraigo el costo de envasado

    buscoSolucionInicial()  # busco una solucion inicial sin usar programacion lineal
    optimizar()
    finTiempo = time.time()  # creo una variable del momento que termine de ejecutar todo
    global tiempoEjecucion  # creo una global para guardar el tiempo de ejecucion
    tiempoEjecucion = finTiempo - inicioTiempo
    generarArchivoResultadosPrimal()  # genero resultados primal


def generarArchivoResultadosPrimal():
    # llamo a las globales
    global resultadosValores
    global sumaRequeridas
    global valorObjetivo
    global cantidadRemediosTotales
    print("Tiempo de ejecución en segundos: ", tiempoEjecucion)
    pathArchivo = os.getcwd() + "/caso_10.out"
    contador = 0
    # valido que no exista un archivo con ese nombre en el directorio, sino voy agregandole un numero
    while (os.path.exists(pathArchivo)):
        contador += 1
        pathArchivo = os.getcwd() + "/" + "Resultados" + str(contador) + ".txt"
    # abro el archivo
    file = open(pathArchivo, "w")
    # escribo los resultados hallados
    file.write("Suma de requeridas: " + str(sumaRequeridas) + os.linesep)
    file.write("Valor Objetivo: " + str(valorObjetivo) + os.linesep)
    file.write("Tiempo de Ejecución: " + str(tiempoEjecucion) + " segundos" + os.linesep)
    file.write("Resultados obtenidos: " + os.linesep)
    file.write(resultadosValores + os.linesep)
    # cierro el archivo
    file.close()
    print("Archivo de resultados: " + pathArchivo)




def extraerFactorInestabilidadMax():
    global factorMax
    factorMax = 0
    for r in remediosConfiguracion:
        for config in remediosConfiguracion[r]:
            if (config == "Factor de inestabilidad maxima"):
                factorMax = remediosConfiguracion[r][config]


def extraerCostoEnvasado():
    global costoEnvasado
    costoEnvasado = 0
    for r in remediosConfiguracion:
        for config in remediosConfiguracion[r]:
            if (config == "costo del envase"):
                costoEnvasado = remediosConfiguracion[r][config]


def leerDatos():
    # Lee el input del usuario y lo manda a la funcion principal Coctel para que haga el resto del trabajo
    # creo variables globales a usar luego
    global drogasDisponibles
    global remediosConfiguracion
    global cotaMaximaCantRemedios
    global minimoDrogasPorRemedio
    global listaRemedios
    drogasDisponibles = {}
    remediosConfiguracion = {}
    cotaMaximaCantRemedios = 0
    listaRemedios = []
    # solicito ele ingreso de los inputs
    rutaFarmaciaTxt = input("Ingrese la ruta del archivo de texto (Drogas):")
    rutaRemedioNuevoTxt = input("Ingrese la ruta del archivo de texto (Remedios):")

    # creo una variable con los 10 minutos solicitados para este TP
    minutos = 60 * 10
    # seteo el metodo que sera temporizado por 10 minutos. En este caso Coctel()
    p = multiprocessing.Process(target=Coctel, name="Coctel", args=(rutaFarmaciaTxt, rutaRemedioNuevoTxt,))
    # doy inicio al temporizador
    p.start()
    # seteo que sea por 10 minutos
    p.join(minutos)
    # si al pasar 10 minutos el proceso sigue vivo lo termino y muestro un print por pantalla
    if p.is_alive():
        p.terminate()
        p.join
        print("Han pasado 10 minutos de ejecucion. Se corto la misma.")


# Métodos encargados de la lectura de archivos

def generarParametros(z, t):
    VarOrigen = z
    count = 0
    params = ""
    cv = {}
    cvinterna = {}

    params = ''.join(VarOrigen.replace(": ", ":"))
    params = ''.join(VarOrigen.replace("\n", ""))

    if t == 'd':
        clavep, valorp = params.split(":")
        lista = valorp.split(",")
        Claves = ['costo unitario', 'unidades requeridas', 'factor de inestabilidad']

        for s in lista:
            params2 = ''.join(s)
            #            clave2, valor2 = params2.split(',')
            cvinterna[Claves[count]] = int(params2) if int(float(params2)) == float(params2) else float(params2)
            count = count + 1

        cv[clavep.strip()] = cvinterna
    else:
        lista = params.split(",")
        Claves = ['costo del envase', 'Factor de inestabilidad maxima']

        for s in lista:
            params2 = ''.join(s)
            #            clave2, valor2 = params2.split(',')
            cvinterna[Claves[count]] = int(params2) if int(float(params2)) == float(params2) else float(params2)
            count = count + 1

        cv[NombreFileEntrada] = cvinterna

    return cv


def leerTxtRemedioNuevo(ruta_txt):
    print("Leyendo: " + ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro = ''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:
            global remedioNuevo
            remedioNuevo = {}
            cont = 0

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n':
                    if f.__contains__("drogas") or f.__contains__("DROGAS"):
                        tipoParametro = 'd'
                        cont = 1
                    elif f.__contains__("remedios") or f.__contains__("REMEDIOS"):
                        tipoParametro = 'r'
                        cont = 1

                    if cont == 0:
                        if tipoParametro == 'r':
                            remedioNuevo.update(generarParametros(f, tipoParametro))
                            break


    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()


def leerTxtFarmacia(ruta_txt):
    print("Leyendo: " + ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro = ''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:

            cont = 0

            if NombreFileEntrada.__contains__("droga") or NombreFileEntrada.__contains__("DROGA") or NombreFileEntrada.__contains__("caso") or NombreFileEntrada.__contains__("CASO"):
                tipoParametro = 'd'
                cont = 1
            elif NombreFileEntrada.__contains__("remedio") or NombreFileEntrada.__contains__("REMEDIO"):
                tipoParametro = 'r'
                cont = 1

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n':

                    if cont == 0:
                        if tipoParametro == 'd':
                            drogasDisponibles.update(generarParametros(f, tipoParametro))
                        else:
                            remediosConfiguracion.update(generarParametros(f, tipoParametro))



    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()

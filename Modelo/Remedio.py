class Remedio:
    nombre = ""
    drogas = []

    def __init__(self):
        return

    def __str__(self):
        drogasLista = ""
        for droga in self.drogas:
            drogasLista += droga.__str__() + ", "
        return str("{nombre remedio: " + self.nombre + ", drogas: " + drogasLista + "}")

    def setNombre(self, nombre):
        self.nombre = nombre

    def setDrogas(self, drogas):
        self.drogas = drogas

    def getNombre(self):
        return self.nombre

    def getDrogas(self):
        return self.drogas

    def getCosto(self):
        costoTotal = 0
        for droga in self.drogas:
            costoTotal += droga.getCosto()
        return costoTotal

    def cantidadDroga(self, drogaEvaluar):
        for droga in self.drogas:
            if (droga.__eq__(drogaEvaluar)):
                return 1
        return 0


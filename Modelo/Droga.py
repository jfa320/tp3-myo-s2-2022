class Droga:
    nombre = ""
    costo = 0
    factorInestabilidad = 0
    unidadesRequeridas = 0

    def __init__(self):
        return

    def __str__(self):
        return str(
            "{nombre droga: " + str(self.nombre) + " ,costo: " + str(self.costo) + " ,factor inestabilidad:" + str(
                self.factorInestabilidad) + " ,unidades requeridas: " + str(self.unidadesRequeridas) + "}")

    def setNombre(self, nombre):
        self.nombre = nombre

    def setCosto(self, costo):
        self.costo = costo

    def setFactorInestabilidad(self, factorInestabilidad):
        self.factorInestabilidad = factorInestabilidad

    def setUnidadesRequeridas(self, unidadesRequeridas):
        self.unidadesRequeridas = unidadesRequeridas

    def getNombre(self):
        return self.nombre

    def getCosto(self):
        return self.costo

    def getFactorInestabilidad(self):
        return self.factorInestabilidad

    def getUnidadesRequeridas(self):
        return self.unidadesRequeridas

    def __eq__(self, other):
        if not isinstance(other, Droga):
            return NotImplemented
        return self.nombre == other.nombre and self.costo == other.costo and self.factorInestabilidad == other.factorInestabilidad and self.unidadesRequeridas == other.unidadesRequeridas

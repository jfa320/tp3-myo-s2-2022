from Modelo import Remedio
class RemedioCantidad:
    remedio = Remedio.Remedio()
    cantidad = 0

    def __init__(self):
        return

    def getRemedio(self):
        return self.remedio

    def getCantidad(self):
        return self.cantidad

    def setRemedio(self, remedio):
        self.remedio = remedio

    def setCantidad(self, cantidad):
        self.cantidad = cantidad